import request from 'axios';

const SERVICE_BASE = "http://localhost:8080/health-check";

const getGroupAsArray = (trees) => {
    const treeArray = [];
    Object.keys(trees).forEach((key) => {
            const group = {name: key, count: trees[key]};
            treeArray.push(group);
        }
    );
    return treeArray
};

export const getGroupedTrees = (lat, log, radius) =>
    request
        .get(`${SERVICE_BASE}/find-grouped-tree/${lat}/${log}/${radius}`)
        .then(response => getGroupAsArray(response.data));

