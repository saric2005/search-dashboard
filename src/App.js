import React, {Component} from 'react';
import './App.css';
import {Button, Heading, Table, TableBody, TableHead, TableRow, TextInput, TextTableCell, TextTableHeaderCell} from 'evergreen-ui';
import * as apis from './apiClient';

class App extends Component {

    state = {
        trees: [],
        latitude: '40.72309177',
        longitude: '-73.84421522',
        radius: '1000',
    };

    componentDidMount() {
        this.search();
    }

    search = () => {
        const {latitude, longitude, radius} = this.state;
        apis.getGroupedTrees(latitude, longitude, radius).then(trees => {
            this.setState({
                trees
            })
        });
    };

    render() {

        const {trees, latitude, longitude, radius} = this.state;
        return (
            <div className="App">

                <div className="search-pane">
                    <TextInput
                        name="text-input-lat"
                        placeholder="Latitude..."
                        onChange={e => this.setState({latitude: e.target.value})}
                        className="search-paneinput"
                        value={latitude}
                    />
                    <TextInput
                        name="text-input-long"
                        placeholder="Longitude..."
                        onChange={e => this.setState({longitude: e.target.value})}
                        className="search-paneinput"
                        value={longitude}
                    />
                    <TextInput
                        name="text-input-radius"
                        placeholder="Radius in meters..."
                        onChange={e => this.setState({radius: e.target.value})}
                        className="search-paneinput"
                        value={radius}
                    />
                    <Button onClick={this.search}>Search</Button>
                </div>
                <Heading size={900}>Dashboard</Heading>

                <div className="dashbaordTable">
                    <Table>
                        <TableHead>
                            <TextTableHeaderCell>
                                Common name
                            </TextTableHeaderCell>
                            <TextTableHeaderCell>
                                Count
                            </TextTableHeaderCell>
                        </TableHead>
                        <TableBody height={600}>
                            {trees.map(car => (
                                <TableRow key={car.name} isSelectable>
                                    <TextTableCell>{car.name}</TextTableCell>
                                    <TextTableCell>{car.count}</TextTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default App;
